# Use latest "Long Term Support" version
# see also: /.gitlab/.gitlab-build-docker-image.yml
FROM node:18.14.0-alpine

COPY . /touchdown

WORKDIR /touchdown

RUN ls -al .

RUN npm install
# RUN npm run test

EXPOSE 9000

RUN chmod +x ./script.sh
ENTRYPOINT ["./script.sh"]
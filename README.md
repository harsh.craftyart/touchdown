---
title: Touchdown - Beautiful landing Pages for your Project
---

# Touchdown.md

Welcome to Touchdown, the simple and efficient Static Site Generator for Developers.

Touchdown is the solution for developers who want to present their projects without additional overhead professionally. We utilise your existing README.md as the source, allowing you to stay in your current flow of tools and workflows.

Get your project the recognition it deserves by presenting it on a dedicated and professional website. Let Touchdown simplify the process for you and make your project stand out. Start now and take your project to the next level.

With just one line of CI configuration, you can quickly deploy your landing page directly to your Gitlab or Github page hosting.

**Features:**

- Create a Professional presentation of your project.
- Use the content which already exists in your README.md.
- Great experience out of the box. Touchdown can be installed on Gitlab **completely without** adding a single line of code. Seriously!
- Integrates with Gitlab pages (Github pages coming soon).
- With complete control and extensibility, you can leave quickly and cleanly anytime.
- Open Source and Free (as in Beer 🍺)!

Impress Your Audience with Touchdown

Elevate Your Project with Touchdown

Say goodbye to the hassle of manual design and coding. Let Touchdown do the heavy lifting and make your project stand out. Get started today and give your project the attention it deserves.

This landing page was, of course, created using touchdown. Visit us and contribute if you like:

[![Source Repo](https://img.shields.io/badge/fork%20on-gitlab-important?logo=gitlab)](https://gitlab.com/touchdown-md/touchdown)
[![Gitlab Pipelines](https://gitlab.com/touchdown-md/touchdown/badges/master/pipeline.svg)](https://gitlab.com/touchdown-md/touchdown/-/pipelines)

## Getting Started / How Does Touchdown Work?

Touchdown is a static site generator that utilizes Gatsby, React, and Markdown files to create professional landing pages for your projects. Here's how it works:

1. You create a README.md file for your project using Markdown syntax.
2. Install Touchdown:

   - If you have no CI/CD configuration, add the following URL to your Gitlab Repository configuration:

     ```
     under: Settings -> CI/CD -> General pipelines
     set CI/CD configuration file: https://touchdown.md/touchdown-gitlab-ci.yml
     ```

     <figure>

     ![My foobar image](docs/configuration-screenshot.png "Example Gitlab Configuration")
     </figure>

   - If you already have a `.gitlab-ci.yml` add this sinle line of configuration to it:
     ```
     include: 'https://touchdown.md/touchdown-gitlab-ci.yml'
     ```

3. Touchdown reads the Markdown file and uses Gatsby and React to generate a static HTML, CSS, and JavaScript page.

In summary, Touchdown simplifies the process of creating professional landing pages by using the tools and workflows that you as a developer are already familiar with. By utilizing Gatsby, React, and Markdown files, Touchdown generates static landing pages that are easy to customize and deploy to your hosting platform of choice.

## Websites already using Touchdown

https://touchdown.md, of course ;-)

other pages already using touchdown:

- https://womm.sh

## Basic typography with Touchdown

Since we have created a tool that integrates Bootstrap styles, you can use the classes the framework includes in your own project, we have typography elements like: blockquotes, code, headings, chapters. You will see how this work in the next examples.

### Blockquotes

<blockquote class="blockquote text-right">
  <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
  <footer style="margin-top: 0" class="blockquote-footer">Someone famous in <cite title="Source Title">Source Title</cite></footer>
</blockquote>

<blockquote class="blockquote text-center">
  <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
  <footer class="blockquote-footer" style="margin-top: 0" >Someone famous in <cite title="Source Title">Source Title</cite></footer>
</blockquote>

### Codeblocks

<pre><code>&lt;p&gt;Sample text here...&lt;/p&gt;
&lt;p&gt;And another line of sample text here...&lt;/p&gt;
</code></pre>

To switch directories, type <kbd>cd</kbd> followed by the name of the directory.<br>
To edit settings, press <kbd><kbd>ctrl</kbd> + <kbd>,</kbd></kbd>

## About

Made with ❤️

[Icons Vectors by Vecteezy](https://www.vecteezy.com/vector-art/9225629-commercial-air-plane-landing-illustration-vector-design)

![My foobar image](docs/bar.jpg "A Foo Bar Image")

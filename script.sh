#!/bin/sh
# This script bundles the main operations of Touchdown like to copy the mds, 
# package, JSX files to build the clients' projects, and deploy the generated bundles

echo "Starting Touchdown Build Process"
echo "Step 1: write environment variables -----------------------------------------"
cat > .env.production << EOF
# GATSBY prefix is necessary to inject it into the Gatsby framework
# This token might be required in the future to request information from Gitlab
GATSBY_GITLAB_ACCESS_TOKEN=$CI_JOB_TOKEN"

# set GATSBY_PROJECT_NAME to Gitlab’s Project Name
# GATSBY_PROJECT_NAME will be displayed in the main menu
GATSBY_PROJECT_NAME=$CI_PROJECT_NAME

# set project's id for reference in API calls
GATSBY_PROJECT_ID=$CI_PROJECT_ID
EOF

export GATSBY_PERSONAL_GITLAB_ACCESS_TOKEN=$CI_PERSONAL_GITLAB_ACCESS_TOKEN
export GATSBY_GITLAB_ACCESS_TOKEN=$CI_JOB_TOKEN
export GATSBY_PROJECT_NAME=$CI_PROJECT_NAME
export GATSBY_PROJECT_ID=$CI_PROJECT_ID
export NETLIFY_SITE_ID=$NETLIFY_SITE_ID
export NETLIFY_AUTH_TOKEN=$NETLIFY_AUTH_TOKEN 

echo "Step 2: cat production env file ---------------------------------------------"
cat .env.production


echo "Step 3: move env file -------------------------------------------------------"
cp .env.production /touchdown/.env.production


echo "Step 4: move static assets to build area at /touchdown/static -----------"
# It is optional for a user-project to contain a static assets.
# They which will be picked up by the Gatsby build and deployed unchanged
# to the websites root directory `/`

# ensure static folder exists in user-project

mkdir -p static
# ensure static folder exists in build area
mkdir -p /touchdown/static
# ensure static folder does not exist in build area
rm -rf /touchdown/static
# move static folder to build area
mv static /touchdown/static


echo "Step 5: move project files to build area at /touchdown/src/content ----------"
echo "build area: /touchdown/src/content"
mv -f  * .[^.]* /touchdown/src/content

echo "Step 6: ls directories ------------------------------------------------------"
ls -al /touchdown
ls -al /touchdown/src/content/src
cat    /touchdown/src/content/README.md


echo "Step 7: run touchdown build and move generated fields to -------------------------------------------------"
cd /touchdown/src/content/
npm run build
mv public $CI_PROJECT_DIR/public
ls -al

#npm run build-with-prefix
#mv public $CI_PROJECT_DIR/public-with-prefix

ls -al $CI_PROJECT_DIR/public
#ls -al $CI_PROJECT_DIR/public-with-prefix
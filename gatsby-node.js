/* eslint-disable */

require("dotenv").config({ path: `.env.${process.env.NODE_ENV}` })
const pathDep = require("path")

function arrayBufferToBase64(buffer) {
  const typedArray = new Uint8Array(buffer)
  const stringChar = typedArray.reduce((data, byte) => {
    return data + String.fromCharCode(byte)
  }, "")

  return btoa(stringChar)
}
console.log(process.env)

const accessToken = process.env.CI_PERSONAL_GITLAB_ACCESS_TOKEN

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  let projectInfo
  let base64Avatar
  try {
    let headers = {}
    if (accessToken) {
      headers = { "PRIVATE-TOKEN": accessToken }
    }
    const url = `https://gitlab.com/api/v4/projects/${process.env.GATSBY_PROJECT_ID}`
    const params = {
      method: "GET",
      headers,
    }
    
    if (process.env.GATSBY_PROJECT_ID) {
      projectInfo = await fetch(url, params).then((response) => {
        if (!response.ok) {
          throw Error(
            `status: ${response.status}, message: ${response.statusText}`
            )
          }
          return response.json()
        })
    }

    if (projectInfo?.avatar_url) {
      base64Avatar = arrayBufferToBase64(
        await fetch(projectInfo.avatar_url).then((res) => res.arrayBuffer())
      )
    }

    createPage({
      path: "/project-info",
      component: pathDep.resolve("./src/components/project-info.tsx"),
      context: {
        avatarUrl: projectInfo?.avatar_url,
        projectInfo,
        base64Avatar,
      },
    })
  } catch (error) {
    console.error(error)
  }

  createPage({
    path: "/blogs",
    component: pathDep.resolve("./src/components/blogs.tsx"),
    context: {
      avatarUrl: projectInfo?.avatar_url,
      base64Avatar,
    },
  })

  const remarkFilesResult = await graphql(`
    {
      allMarkdownRemark {
        nodes {
          id
          parent {
            ... on File {
              name
              relativeDirectory
            }
          }
          html
          frontmatter {
            title
          }
        }
      }
    }
  `)

  remarkFilesResult.data.allMarkdownRemark.nodes.forEach((node) => {
    const path =
      node?.parent?.name === "README" && node?.parent?.relativeDirectory === ""
        ? `/`
        : `/${node?.parent?.relativeDirectory}/${node?.parent?.name}.md`
    createPage({
      path,
      component: pathDep.resolve("./src/components/template.tsx"),
      context: {
        avatarUrl: projectInfo?.avatar_url,
        title: node?.frontmatter?.title,
        html: node.html,
        base64Avatar,
      },
    })
  })
}

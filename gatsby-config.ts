import type { GatsbyConfig } from "gatsby"
import * as dotenv from "dotenv"

dotenv.config({ path: `${__dirname}/.env.${process.env.NODE_ENV}` })

console.log("__dirname:" + __dirname)

const config: GatsbyConfig = {
  siteMetadata: {
    title: `web`,
    siteUrl: `https://systemkern.gitlab.io/works-on-my-machine`,
  },

  // More easily incorporate content into your pages through automatic TypeScript type generation and better GraphQL IntelliSense.
  // If you use VSCode you can also use the GraphQL plugin
  // Learn more at: https://gatsby.dev/graphql-typegen
  graphqlTypegen: true,
  pathPrefix:
    typeof process.env.TOUCHDOWN_PATH_PREFIX !== "undefined"
      ? process.env.TOUCHDOWN_PATH_PREFIX
      : "",
  plugins: [
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        gatsbyRemarkPlugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 1200,
            },
          },
        ],
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `content`,
        path: `${__dirname}`,
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 800,
            },
          },
        ],
      },
    },
    `gatsby-plugin-sharp`,
    `gatsby-transformer-sharp`,
    `gatsby-plugin-image`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/assets/images`,
      },
    },
    `gatsby-plugin-sass`,
  ],
}

export default config

import { useQuery } from "react-query"
import { getIssuesList } from "../API/issuesApi"
import { projectId } from "../globalVars"

export const useIssuesInfo = () => {
  const issuesQuery = useQuery(
    ["issues-info", projectId],
    () => {
      if (!projectId) {
        return Promise.reject({})
      }
      return getIssuesList(projectId)
    },
    {
      staleTime: 1000 * 60 * 60,
    }
  )

  return issuesQuery
}

import { ProjectData } from "../domain/interfaces"

export function getProjectInfo(projectId: string): Promise<void | ProjectData> {
  return fetch(`https://gitlab.com/api/v4/projects/${projectId}`)
    .then((response) => response.json())
    .catch((err) => {
      console.error(err)
      throw Error(err)
    })
}

export const nodeEnv = process.env.NODE_ENV
export const personalGitlabAccessToken =
  process.env.CI_PERSONAL_GITLAB_ACCESS_TOKEN
export const gatsbyProjectName = process.env.GATSBY_PROJECT_NAME
export const projectId = process.env.GATSBY_PROJECT_ID
export const isFooterVisible = process.env.GATSBY_FOOTER_VISIBLE === "true"

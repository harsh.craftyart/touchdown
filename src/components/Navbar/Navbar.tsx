import React, { useState } from "react"
import { Link, graphql, useStaticQuery } from "gatsby"
import "./Navbar.scss"
import { GiHamburgerMenu } from "react-icons/gi"
import { gatsbyProjectName } from "../../globalVars"

interface NavbarProps {
  base64Avatar?: string
  avatarUrl?: string
}

const resolveAvatar = (
  base64Avatar?: string,
  avatarUrl?: string,
  projectNameInitialLetter?: string
) => {
  if (base64Avatar) {
    return (
      <img
        className="avatar"
        src={`data:image/png;base64,${base64Avatar}`}
        alt=""
      />
    )
  }

  if (avatarUrl) {
    return <img className="avatar" src={avatarUrl} alt="" />
  }

  return (
    <p className="header-content-link-el-placeholder avatar">
      {projectNameInitialLetter || "P"}
    </p>
  )
}

export const Navbar = ({ base64Avatar, avatarUrl }: NavbarProps) => {
  const [showOptionsMobile, setShowOptionsMobile] = useState(false)
  const query = useStaticQuery(graphql`
    {
      allFile {
        nodes {
          name
          relativeDirectory
        }
      }
    }
  `)

  const options = (
    <ol>
      <li>
        <Link to="/">{gatsbyProjectName || "Home"}</Link>
      </li>
      <li>
        {query?.allFile?.nodes.filter(
          ({ relativeDirectory }: { relativeDirectory: string }) =>
            relativeDirectory === "blog"
        )?.length > 0 && <Link to="/blogs">Blogs</Link>}
      </li>
      <li>
        {query?.allFile?.nodes.filter(
          ({ name }: { name: string }) => name === "contribute"
        )?.length > 0 && <Link to="/contribute.md">contribute</Link>}
      </li>
    </ol>
  )

  return (
    <header className="header">
      <div className="header-content">
        <div className="header-content-link">
          <Link to="/" className="header-content-link-el">
            {resolveAvatar(base64Avatar, avatarUrl, gatsbyProjectName?.at(0))}
          </Link>
        </div>
        <div className="header-content-desktop-options">{options}</div>
        <div className="header-content-mobile-options">
          <button
            className={`icon-btn ${showOptionsMobile ? "active" : ""}`}
            onClick={() => setShowOptionsMobile(!showOptionsMobile)}
          >
            <GiHamburgerMenu className="icon" />
          </button>
          {showOptionsMobile && (
            <div className="header-content-mobile-options-list">{options}</div>
          )}
        </div>
      </div>
    </header>
  )
}

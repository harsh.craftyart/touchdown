import * as React from "react"
import { Helmet } from "react-helmet"
import "../styles/styles.scss"
import { Navbar } from "./Navbar/Navbar"
import { preprocessHtml } from "./templateHelpers"
import { Footer } from "./Footer/Footer"
import { gatsbyProjectName } from "../globalVars"

interface PageContext {
  html: string
  title?: string
  base64Avatar?: string
  avatarUrl: string
}

interface TemplateProps {
  pageContext: PageContext
}

export default function Template(props: TemplateProps) {
  const { pageContext } = props

  const newHtml = preprocessHtml(pageContext.html)
  return (
    <div>
      <Helmet title={pageContext.title || gatsbyProjectName} />
      <Navbar
        base64Avatar={pageContext?.base64Avatar}
        avatarUrl={pageContext?.avatarUrl}
      />
      <main className="main" dangerouslySetInnerHTML={{ __html: newHtml }} />
      <Footer />
    </div>
  )
}

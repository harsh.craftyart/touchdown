import React from "react"
import "./Footer.scss"
import { isFooterVisible } from "../../globalVars"

export const Footer = () => {
  if (!isFooterVisible) {
    return null
  }

  return (
    <div className="footer">
      <a href="https://touchdown.md/">powered by touchdown.md</a>
    </div>
  )
}

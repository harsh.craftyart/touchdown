/* eslint-disable */

import React, { useEffect } from "react"
import { Navbar } from "../components/Navbar/Navbar"
import { graphql, useStaticQuery, withPrefix } from "gatsby"
import { Link } from "@reach/router"
import { GatsbyImage } from "gatsby-plugin-image"
import "./blogs.scss"

interface BlogProps {
  pageContext: {
    base64Avatar?: string
    avatarUrl?: string
  }
}

export default (props: BlogProps) => {
  const { pageContext } = props

  const fileQuery = useStaticQuery(graphql`
    query {
      allImageSharp {
        nodes {
          fixed {
            originalName
          }
          internal {
            content
          }
          gatsbyImageData
          id
        }
      }
      allFile {
        nodes {
          relativePath
          relativeDirectory
          extension
          name
          childrenMarkdownRemark {
            frontmatter {
              title
            }
          }
        }
      }
    }
  `)

  const blogs = fileQuery.allFile.nodes.filter(
    (file) => file.extension === "md" && file.relativeDirectory === "blog"
  )

  const images = fileQuery.allImageSharp.nodes

  const { base64Avatar, avatarUrl } = pageContext

  return (
    <div className="blogs">
      <Navbar base64Avatar={base64Avatar} avatarUrl={avatarUrl} />
      <main className="blogs-main">
        <h1>Blogs list</h1>
        <div className="blogs-main-cards">
          {blogs.length > 0 ? (
            blogs?.map(
              ({
                name,
                childrenMarkdownRemark: [
                  {
                    frontmatter: { title },
                  },
                ],
              }: {
                childrenMarkdownRemark: any
              }) => {
                const previewImage = images?.find(
                  (image) => image?.fixed?.originalName === `${name}.png`
                )

                return (
                  <div className="blog-card" key={`blog-${name}`}>
                    <Link to={withPrefix(`/blog/${name}.md/`)}>
                      <p>{title}</p>
                      <GatsbyImage image={previewImage?.gatsbyImageData} />
                    </Link>
                  </div>
                )
              }
            )
          ) : (
            <div>
              <p>No blogs to render</p>
            </div>
          )}
        </div>
      </main>
    </div>
  )
}

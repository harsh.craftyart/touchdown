---
title: "How to create a project from scratch"
---

# How to create a project from scratch

In the dynamic landscape of web development, creating visually appealing and functional web pages can be a time-consuming and intricate process. Enter Touchdown, your ultimate solution for streamlining the creation and customization of web pages. Touchdown empowers you, the customer, to effortlessly transform your markdown files into stunning and polished web pages, saving you valuable time and effort.

With Touchdown, the complexities of infrastructure management fade into the background. Touchdown works tirelessly behind the scenes, handling the intricate technical details so you can focus on what you do best – shaping your content and crafting your message. No longer will you need to divert your attention from your speciality to grapple with infrastructure concerns.

After the introduction about what Touchdown can do, we are now going to provide a step-by-step guide to quickly develop a site that renders in Gitlab pages.

First of all you need to create a repository in Gitlab that will run your pipelines and store your information and content.

1. In Gitlab there is plus icon in the top-left corner that displays a list of options, click on “Create repository”, and you will see the next view:

<div class="image-container">

![how-to-create-a-project-from-scratch](../src/assets/images/how-to-create-a-project-from-scratch.png)

</div>

2. Next, click on “Create blank project“ and then you will be redirected to the blank project creation form:

<div class="image-container">

![creation_form](../src/assets/images/creation_form.png)

</div>

Here you are free to type what you want for name, the project url depends on you namespace, I suggest to pick the yours for simplicity. In visibility level the choice is also up to you, you can choose public if you want everybody to see your repository files as thery are going to be published anyway.

I will create a README file because it is going to be useful as landing page for our new project.

Last but not least, click on the “Create project” button to submit the form. This project will be available for anybody so you can have it as example as well as the tutorial.

Ok, so now the repository already exist:

<div class="image-container">

![repo_overview](../src/assets/images/repo_overview.png)

</div>

NOTE:
It's important to mention that our build will read a variable called `CI_PERSONAL_GITLAB_ACCESS_TOKEN`, in case your repository is private you will need to set this var in the environmental vars of the Gitlab project, the value is a token that you can generate with the next steps:

- Click on `Access tokens`
<div class="image-container">

![Alt text](../src/assets/images/token_options.png)

</div>

- Click on `Add new token`
- Now you just have to type a name for your token, also the date which has to be whithin a year from the moment you try to generate it, and it's done, copy the token somewhere as you will not see it again.

<div class="image-container">

![Alt text](../src/assets/images/fill_token_fields_out.png)

</div>

- Click on CI/CD, expand variable options and you can add the variable with the name the you saw in the first step and the value is the token you jut copied.

<div class="image-container">

![Alt text](../src/assets/images/add_env_var_menu.png)

</div>

3. The next step will be to clone this repository to our local files system, and then the local repository and the remote will be connected.

To clone the repository all you need to do is to click on the clone drop-down and then copy the content of the SSH input:

<div class="image-container">

![repo_overview](../src/assets/images/clone_dropdown.png)

</div>

In this tutorial we assume you know how to use SSH repositories, if not you can visit the next post for more information:

[Use SSH keys to communicate with GitLab | GitLab](https://docs.gitlab.com/ee/user/ssh.html)

4. next you only paste the link in your terminal and hit enter:

<div class="image-container">

![clone_locally](../src/assets/images/clone_locally.png)

</div>

the repository is now in your local.

5. Open this new folder in a text editor like Visual Studio Code to ease the content creation, and then you create a file called “.gitlab-ci.yml”, this file is the key of everything, it will copy the gitlab file already handcrafted by us into your project before pipeline process has begun and do the required steps to build the pages from your markdown files under the hood, so you can focus on writing content and customizing your site instead of struggling with Gatsby and Gitlab by your own.

6. After that we only have to commit this file and push to the repository:

<div class="image-container">

![copy_reference](../src/assets/images/copy_reference.png)

</div>

Finally your repository should be live now in Gitlab pages as you see in the next picture:

<div class="image-container">

![created_project](../src/assets/images/created_project.png)

</div>

You can focus on adding content to your site, for example creating new blogs or pages from markdown. We hope you enjoy the experience of building you site.

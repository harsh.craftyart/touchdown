---
title: "How to add bootstrap elements to your page"
---

# How to add bootstrap elements to your page

In this tutorial we are going to show you step-by-step the process to add Boostrap elements to your page.
Touchdown has already included the minified and optimized bundles for React, which means that you will not need to pass through any process of installation or configuration.

The only thing you need to do is:

1. Go to the Boostrap official webpage: https://getbootstrap.com/

2. You will see a blogs tab in the menu of the page, click on it to open the page

<div class="image-container">

![Alt text](../src/assets/images/how-to-add-bootsrap-elements.png)

</div>

3. Now you find plenty of code blocks that you can use as an example, just copy and paste one of them in plain HTML.

Touchdown is powered by `gatsby-transformer-remark` and `gatsby-plugin-mdx`, which are powerful plugins that transform MD files into HTML, they also allow us to embed HTML into MD, so if you paste HTML code you will see the same DOM elements in the browser as with a normal web project.

For example, the next dropdown code from the blogs page:

```
<div class="dropdown" data-bs-theme="light">
 <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButtonLight" data-bs-toggle="dropdown" aria-expanded="false">
   Default dropdown
 </button>
 <ul class="dropdown-menu" aria-labelledby="dropdownMenuButtonLight">
   <li><a class="dropdown-item active" href="#">Action</a></li>
   <li><a class="dropdown-item" href="#">Action</a></li>
   <li><a class="dropdown-item" href="#">Another action</a></li>
   <li><a class="dropdown-item" href="#">Something else here</a></li>
   <li><hr class="dropdown-divider"></li>
   <li><a class="dropdown-item" href="#">Separated link</a></li>
 </ul>
</div>

```

will yield the next:

<div class="image-container">

![Alt text](../src/assets/images/bootstrap-dropdown.png)

</div>

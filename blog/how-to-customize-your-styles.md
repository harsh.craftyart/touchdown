---
title: "How to customize your styles"
---

![how-to-customize-your-styles](../src/assets/images/how-to-customize-your-styles.png)

# How to customize you styles

In the process of designing your place, you might want to adapt the styles currently existing to your organization or
personal preferences, doing so should be a very straight-forward process that will require only some concepts to understand

First of all, the Touchdown project has defined HTML and CSS structures and variables that are not very rigid and hence can be overwritten.

## Overwrite colors or styles of HTML tags

An HTML tag means an element that can be represented and shaped in the DOM structure.
So you might want to change for example the color of the h1, h2, p or div tags and so one.
Doing this only requires that you create a file called `touchdown.css` that touchdown will import in the template for each article and then these styles will be available globally.

```
h1 {
  color: red !important;
}
```

```
p {
    font-size: 5rem !important;
}
```

## Overwrite colors or styles of HTML tags

You might also wish to change the styles based on classes that we have defined, so you only need to find the CSS class that you will overwrite and add the important directive of CSS like:

```
.header {
  background-color: rgb(33, 150, 243) !important;
}
```

Finally, this is the handling of styles for the simplest cases. In a further tutorial we will include advanced tricks for overwritting CSS variables of root colors and classes so you will have more tools to customize your pages completely, also the bootstrap components and styles available in the project, for the moment you can experiment with the showed techniques.
